<?php

  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
  
  // $rest_json = file_get_contents("php://input");
  // $_POST = json_decode($rest_json, true);

  $dbServername = "localhost";
  $dbUsername = "root";
  $dbPassword = "";
  $dbName = "firstprojectcrud";
  
  $mysqli = new mysqli('localhost','root','','firstprojectcrud');
  $myArray = array();


  if (mysqli_connect_errno()) {
    return "failed to connect to mysql db: " . mysqli_connect_errno();
  } else {
    if ($result = $mysqli->query("SELECT * FROM `registeredusers`;")) {

      while($row = mysqli_fetch_assoc($result)) {
              $myArray[] = $row;
      }
      echo json_encode($myArray);
      $result->close();
    }
  }

  $mysqli->close();

?>