<?php

  header('Content-Type: application/json');
  header('Access-Control-Allow-Origin: *');
  

  $aResult = array();
  
  $rest_json = file_get_contents("php://input");
  $_POST = json_decode($rest_json, true);
  

  if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

  if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

  if( !isset($aResult['error']) ) {

    switch($_POST['functionname']) {
      case 'insertNewUser':
        if( !is_array($_POST['arguments']) || (count($_POST['arguments']) < 4) ) {
          $aResult['error'] = 'Error in arguments!';
        }
        else {
          $aResult['result'] = insertNewUser($_POST['arguments'][0], $_POST['arguments'][1], $_POST['arguments'][2], $_POST['arguments'][3]);
        }
        break;
      case 'deleteUser':
        $aResult['result'] = deleteUser($_POST['arguments']["fn"], $_POST['arguments']["ln"], $_POST['arguments']["ph"]);
        break;
      case 'updateUser':
        $aResult['result'] = updateUser($_POST['arguments']["ofn"], $_POST['arguments']["oln"], $_POST['arguments']["oph"], $_POST['arguments']["nfn"], $_POST['arguments']["nln"], $_POST['arguments']["nph"]);
        break;
      default:
        $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
        break;
    }

  }

  echo json_encode($aResult);

  function updateUser($oldfirstName, $oldlastName, $oldphone, $newfirstName, $newlastName, $newphone) {
    $dbServername = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "firstprojectcrud";
    
    $con = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);
    if (mysqli_connect_errno()) {
      return "failed to connect to mysql db: " . mysqli_connect_errno();
    } else {
      $query = "UPDATE `registeredusers` SET `firstname`=" . "'" . $newfirstName . "'" . ",`lastname`=" . "'" . $newlastName . "'" . ",`phone`=" . "'" . $newphone . "'" . " WHERE `firstname`=" . "'" . $oldfirstName . "'" . " AND `lastname`=" . "'" . $oldlastName . "'" . " AND `phone`=" . "'" . $oldphone . "'" . ";"; 
      mysqli_query($con, $query);
      mysqli_close($con);
      return [$newfirstName, $newlastName, $newphone, $query];
    }
  }

  function deleteUser($firstName, $lastName, $phone) {
    $dbServername = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "firstprojectcrud";
    
    $con = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);
    if (mysqli_connect_errno()) {
      return "failed to connect to mysql db: " . mysqli_connect_errno();
    } else {
      $query = "DELETE FROM `registeredusers` WHERE firstname=" . "'" . $firstName . "'" . " AND lastname=" . "'" . $lastName . "'" . " AND phone=" . "'" . $phone . "'" . ";"; 
      mysqli_query($con, $query);
      mysqli_close($con);
      return $query;
    }
  }

  function insertNewUser($firstName, $lastName, $phone, $dateReg) {
    $dbServername = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "firstprojectcrud";
    
    $con = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);
    if (mysqli_connect_errno()) {
      return "failed to connect to mysql db: " . mysqli_connect_errno();
    } else {
      $query = "INSERT INTO `registeredusers` (`firstname`, `lastname`, `phone`, `dateregistered`) VALUES (" . "'" . $firstName . "'" . ", " . "'" . $lastName . "'" . ", " . "'" . $phone . "'" . ", " . "'" . $dateReg . "'". ");";
      mysqli_query($con, $query);
      mysqli_close($con);
      return [$firstName, $lastName,$phone];
    }

  }

?>