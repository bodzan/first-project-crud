function easyHTTP() {
  this.http = new XMLHttpRequest();
}

// Make an HTTP GET Request
easyHTTP.prototype.get = function(url, callback) {
  this.http.open('GET', url, true);

  let self = this;
  this.http.onload = function() {
    if(self.http.status === 200) {
      callback(null, self.http.responseText);
    } else {
      callback('Error: ' + self.http.status);
    }
  }

  this.http.send();
}

// Make an HTTP POST Request
easyHTTP.prototype.post = function(url, data, callback) {
  this.http.open('POST', url, true);
  this.http.setRequestHeader('Content-type', 'application/json');
  

  let self = this;
  this.http.onload = function() {
    callback(null, self.http.responseText);
  }
  
  this.http.send(JSON.stringify(data));
}


// Make an HTTP PUT Request
easyHTTP.prototype.put = function(url, data, callback) {
  this.http.open('PUT', url, true);
  this.http.setRequestHeader('Content-type', 'application/json');

  let self = this;
  this.http.onload = function() {
    callback(null, self.http.responseText);
  }

  this.http.send(JSON.stringify(data));
}

// Make an HTTP DELETE Request
easyHTTP.prototype.delete = function(url, callback) {
  this.http.open('DELETE', url, true);

  let self = this;
  this.http.onload = function() {
    if(self.http.status === 200) {
      callback(null, 'Post Deleted');
    } else {
      callback('Error: ' + self.http.status);
    }
  }

  this.http.send();
}


const http = new easyHTTP;

document.getElementById("action").addEventListener('click', function() {
  var fn = document.getElementById('firstname').value;
  var ln = document.getElementById('lastname').value;
  var ph = document.getElementById('phone').value;

  if (fn === "" || ln === "" || ph === "") {
    alert("All fields must be entered!")
    return;
  }
  var d = new Date();
 
  var curr_date = d.getDate();

  var curr_month = d.getMonth();

  var curr_year = d.getFullYear();

  var curr_hour = d.getHours();
  var curr_min = d.getMinutes();
  var datestring = curr_date + "-" + curr_month + "-" + curr_year + " " + curr_hour + ":" + curr_min;
  var data = {arguments : [fn, ln, ph, datestring], functionname: "insertNewUser"};
  // console.log(data);
  http.post('http://localhost/FirstProjectCRUD/Backend/functionHandler.php', data, function(err, post) {
    if (err) {
      console.log(err);
    } else { 
      console.log(post);
      var result = JSON.parse(post);
      document.getElementById('firstname').value = "";
      document.getElementById('lastname').value = "";
      document.getElementById('phone').value = "";
      var t = document.getElementById("table");
      var r = t.insertRow(-1);
      var c = r.insertCell(0);
      c.innerHTML = result.result[0];
      c = r.insertCell(1);
      c.innerHTML = result.result[1];
      c = r.insertCell(2);
      c.innerHTML = result.result[2];
      c = r.insertCell(3);
      c.innerHTML = "<button name=\"edit\" data-rownum=\"\" class=\"btn waves-effect waves-light\"> <i class=\"material-icons\"> edit </i> </button> <button name=\"delete\" data-rownum=\"\" class=\"btn waves-effect waves-light\"> <i class=\"material-icons\">clear </i> </button>";
      updateRowIds();
      setDeleteButtonFunction();
      setEditButtonFunction(); 
    }
  });

});


document.getElementById("update").addEventListener('click', function() {
  var table = document.getElementById("table");
  var updateRowId = window.localStorage.getItem("updateRowId");
  var oldfn = table.childNodes[0].childNodes[updateRowId].childNodes[0].textContent;
  var oldln = table.childNodes[0].childNodes[updateRowId].childNodes[1].textContent;
  var oldph = table.childNodes[0].childNodes[updateRowId].childNodes[2].textContent;
  var fn = document.getElementById('ufirstname').value;
  var ln = document.getElementById('ulastname').value;
  var ph = document.getElementById('uphone').value;

  updateData = {arguments: {ofn:oldfn, oln:oldln, oph:oldph, nfn:fn, nln:ln, nph:ph}, functionname:"updateUser"};
  console.log(updateData);

  if (fn === "" || ln === "" || ph === "") {
    alert("All fields must be entered!")
    return;
  }

  // console.log(data);
  http.post('http://localhost/FirstProjectCRUD/Backend/functionHandler.php', updateData, function(err, post) {
    if (err) {
      console.log(err);
    } else { 
      console.log(post);
      var result = JSON.parse(post);
      table.childNodes[0].childNodes[updateRowId].childNodes[0].textContent = result.result[0];
      table.childNodes[0].childNodes[updateRowId].childNodes[1].textContent = result.result[1];
      table.childNodes[0].childNodes[updateRowId].childNodes[2].textContent = result.result[2];

    }
  });
  document.getElementById("updateform").setAttribute("hidden", "");
});


document.addEventListener("DOMContentLoaded", function() {
  // console.clear();
  document.getElementById('firstname').value = "";
  document.getElementById('lastname').value = "";
  document.getElementById('phone').value = "";
  getUsers();
})

function getUsers() {
  
   http.get('http://localhost/FirstProjectCRUD/Backend/getUsers.php', function(err, users) {
    if (err) {
      console.log(err);
    } else { 
      var userArray = JSON.parse(users);
      var c, r, t;
      t = document.createElement('table');
      r = t.insertRow(0); 
      c = r.insertCell(0);
      c.innerHTML = "First Name";
      c = r.insertCell(1);
      c.innerHTML = "Last Name";
      c = r.insertCell(2);
      c.innerHTML = "Phone number";
      c = r.insertCell(3);
      c.innerHTML = "";
      userArray.forEach(function(user, index) {
        r = t.insertRow(index + 1);
        c = r.insertCell(0);
        c.innerHTML = user.firstname;
        c = r.insertCell(1);
        c.innerHTML = user.lastname;
        c = r.insertCell(2);
        c.innerHTML = user.phone;
        c = r.insertCell(3);
        c.innerHTML = "<button name=\"edit\" data-rownum=" + (index+1) + " class=\"btn waves-effect waves-light\"> <i class=\"material-icons\"> edit </i> </button> <button name=\"delete\" data-rownum=" + (index+1) + " class=\"btn waves-effect waves-light\"> <i class=\"material-icons\">clear </i> </button>";
        // editBtn = document.createElement("button").setAttribute("class", "btn waves-effect waves-light");
        // editBtn.innerHTML = document.createElement("i").setAttribute("class", "material-icons")
        // deleteBtn = document.createElement("button").setAttribute("class", "btn waves-effect waves-light");
      });
      t.setAttribute("id", "table");
      document.getElementById("tablediv").appendChild(t);
      
      setDeleteButtonFunction();
      setEditButtonFunction();
    }
  });
}


function setDeleteButtonFunction() {
  var deleteBtns = document.getElementsByName("delete");

  deleteBtns.forEach(element => {
    element.addEventListener("click", function(){
      var deleteRowId = this.getAttribute("data-rownum");
      var table = document.getElementById("table");
      var rows = table.childNodes[0].childNodes;
      data = {arguments:{fn:rows[deleteRowId].childNodes[0].textContent,ln: rows[deleteRowId].childNodes[1].textContent, ph:rows[deleteRowId].childNodes[2].textContent}, functionname:"deleteUser"};
      console.log(data);
      http.post('http://localhost/FirstProjectCRUD/Backend/functionHandler.php', data, function(err, post) {
        if (err) {
          console.log(err);
        } else { 
          console.log(post);
        }
      });

      rows[deleteRowId].remove();
      updateRowIds();
      })
  });

}


function setEditButtonFunction() {
  var editBtns = document.getElementsByName("edit");
  editBtns.forEach(element => {
    element.addEventListener("click", function(){
      var updateRowId = this.getAttribute("data-rownum");
      var table = document.getElementById("table");
      var rows = table.childNodes[0].childNodes;
      var updatediv = document.getElementById("updateform");

      if (updatediv.hasAttribute("hidden")){
        updatediv.removeAttribute("hidden");
      }
      window.localStorage.setItem("updateRowId", updateRowId);
      document.getElementById('ufirstname').value = rows[updateRowId].childNodes[0].textContent;
      document.getElementById('ulastname').value = rows[updateRowId].childNodes[1].textContent;
      document.getElementById('uphone').value = rows[updateRowId].childNodes[2].textContent;
      })
  });

}


function updateRowIds() {
  var deleteBtns = document.getElementsByName("delete");
  var editBtns = document.getElementsByName("edit");
  var i = 1;
  deleteBtns.forEach(element => {
    element.setAttribute("data-rownum", i);
    i++;
  });
  i=1;
  editBtns.forEach(element => {
    element.setAttribute("data-rownum", i);
    i++;
  });
}